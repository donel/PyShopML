const isEven = require('./is-even')

test('3 is not even to two', () => {
  expect(isEven(3)).toBe(false)
})

/*
Expected: false; Received: 1
Difference: 
Comparing two different types of values. Expected boolean but received number.
*/
