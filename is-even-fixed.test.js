const isEven = require('./is-even-fixed')

test('3 is not even to two', () => {
  expect(isEven(3)).toBe(false)
})

//PASS  ./is-even-fixed.test.js
