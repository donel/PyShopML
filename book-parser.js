const fs = require('fs'),
  books = fs.readFileSync('./books/books.csv', 'utf-8').replace(/; /g, ';'),
  rows = [...Array(books.match(/\n/g).length + 1).keys()].slice(1),
  cols = [...Array(books.split('\n')[0].split(';').length).keys()],
  books2json = { authors: [] },
  parser = (col, row) =>
    books
      .split('\n')
      .filter((el, index) => index === row)[0]
      .split(';')[col]

for (let row of rows) {
  let book = {}
  let author = ''
  for (let col of cols) {
    switch (col) {
      case 0:
        book.title = parser(col, row)
        break
      case 2:
        book.description = parser(col, row)
        break
      case 1:
        author = parser(col, row)
        const uniqueId = books2json.authors.findIndex(
          el => el.author === author
        )
        if (uniqueId == -1) {
          books2json.authors.push({ author, books: [book] })
        } else {
          books2json.authors[uniqueId].books.push(book)
        }
        break
    }
  }
}

fs.writeFileSync('./books/books.json', JSON.stringify(books2json, '', 2))
